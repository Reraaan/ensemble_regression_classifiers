import argparse
import pandas as pd
import numpy as np
#  import matplotlib.pyplot as plt

from sklearn.model_selection import LeaveOneOut
from sklearn.metrics import roc_auc_score, precision_score, recall_score, accuracy_score
from sklearn.linear_model import Ridge


class Sample(object):
    type_ = 'Sample'

    def __init__(self, data):
        self.features = data.iloc[2:].values
        self.score = data['Score']
        self.positive = self.score > 50
        self.name = data['Name']

    def __str__(self):
        return '{} ({} {})'.format(self.type_, self.name, self.score)

    def __repr__(self):
        return str(self)


class CellLine(Sample):
    type_ = 'Cell line'


class Patient(Sample):
    type_ = 'Patient'


class Classifier(object):

    def __init__(self, cell_lines, patient):
        self.M = self.build_model(cell_lines)
        self.f = self.M.predict([patient.features])[0]
        self.positive = patient.positive
        self.r = 1. if self.positive else -1.

    def build_model(self, cell_lines):
        model = Ridge()
        model.fit(
            np.array([c.features for c in cell_lines]),
            np.array([c.score for c in cell_lines]),
        )
        return model

    def step(self, x):
        # return np.heaviside(x, 1)
        return 1. / (1. + np.exp(-2*x))

    def vote(self, patient):
        return self.r * self.step(self.r * (self.M.predict([patient.features])[0] - self.f))


class Ensemble(object):

    def __init__(self, cell_lines, patients, nearest_count):
        self.clfs = []
        for patient in patients:
            clf = Classifier(self.get_nearest_cell_lines(cell_lines, patient, nearest_count), patient)
            if 0 <= clf.f <= 100:
                self.clfs.append(clf)
        self.weights = [1. for clf in self.clfs]
        posw = 0
        negw = 0
        for i, clf in enumerate(self.clfs):
            if clf.positive:
                posw += self.weights[i]
            else:
                negw += self.weights[i]
        for i, clf in enumerate(self.clfs):
            if clf.positive:
                self.weights[i] /= posw
            else:
                self.weights[i] /= negw

    @staticmethod
    def get_nearest_cell_lines(cell_lines, patient, nearest_count):
        cell_lines.sort(key=lambda cl: np.linalg.norm(cl.features - patient.features))
        return cell_lines[:nearest_count]

    def predict(self, patient):
        votes = np.array([clf.vote(patient) for clf in self.clfs])
        return 50 * (np.sum(self.weights * votes) + 1.)


def calc_fpr(targets, classes):
    total_negative = sum([not target for target in targets])
    return sum([not target and cls for target, cls in zip(targets, classes)]) / float(total_negative)


def calc_fnr(targets, classes):
    total_positive = sum([target for target in targets])
    return sum([target and not cls for target, cls in zip(targets, classes)]) / float(total_positive)


def find_optimal_tau(targets, predicts):
    ordered_predicts, ordered_targets = zip(*sorted(zip(predicts, targets)))
    for tau in ordered_predicts:
        classes = [predict >= tau for predict in predicts]
        fpr = calc_fpr(targets, classes)
        if fpr < 0.1:
            break
    return tau


def calc_roc_auc_and_tau(ensemble, patients):
    positive_count = sum([c.positive for c in ensemble.clfs])
    negative_count = sum([not c.positive for c in ensemble.clfs])
    if positive_count < 3 or negative_count < 3:
        return (0, 0)

    predicts = [ensemble.predict(patient) for patient in patients]
    targets = [patient.positive for patient in patients]
    return roc_auc_score(targets, predicts), find_optimal_tau(targets, predicts)


def main():
    parser = argparse.ArgumentParser(description='EBC')
    parser.add_argument('-c', '--cell-lines', required=True)
    parser.add_argument('-p', '--patients', required=True)
    parser.add_argument('-q', '--min-q', type=int, default=1)
    parser.add_argument('-Q', '--max-q', type=int, default=227)
    params = parser.parse_args()

    cell_lines_data = pd.read_csv(params.cell_lines)
    patients_data = pd.read_csv(params.patients)

    cell_lines = [CellLine(row) for _, row in cell_lines_data.iterrows()]
    patients = [Patient(row) for _, row in patients_data.iterrows()]

    targets = []
    predicts = []
    classes = []
    Qs = range(params.min_q, params.max_q + 1)
    #  Qs = [31, 32, 54]
    for ti in range(len(patients)):
        train_patients = patients[:ti] + patients[ti+1:]
        test_patient = patients[ti]

        aucs = []
        taus = []
        for Q in Qs:
            ensemble = Ensemble(cell_lines, train_patients, Q)
            auc, tau = calc_roc_auc_and_tau(ensemble, train_patients)
            aucs.append(auc)
            taus.append(tau)
        optimal_Q = Qs[np.argmax(aucs)]
        optimal_tau = 50  # taus[np.argmax(aucs)]
        ensemble = Ensemble(cell_lines, train_patients, optimal_Q)
        predict = ensemble.predict(test_patient)
        cls = predict >= optimal_tau

        targets.append(test_patient.positive)
        predicts.append(predict)
        classes.append(cls)
        print(
            '{:3d}: optimal q {:3d}, auc {:4.3f}, predict {:7.3f} ({:1d}), target {:1d}'
            .format(ti, optimal_Q, np.max(aucs), predict, cls, test_patient.positive)
        )

    tau = find_optimal_tau(targets, predicts)
    fpr = calc_fpr(targets, classes)
    fnr = calc_fnr(targets, classes)
    accuracy = accuracy_score(targets, classes)
    precision = precision_score(targets, classes)
    recall = recall_score(targets, classes)
    auc = roc_auc_score(targets, predicts)

    #  print('tau: {}'.format(auc))
    print('fpr: {:4.3f}'.format(fpr))
    print('fnr: {:4.3f}'.format(fnr))
    print('accuracy: {:4.3f}'.format(accuracy))
    print('precision: {:4.3f}'.format(precision))
    print('recall: {:4.3f}'.format(recall))
    print('auc: {:4.3f}'.format(auc))


if __name__ == '__main__':
    main()
